import pandas as pd
import coherence as co
import copy, nltk, pickle, difflib
from features import getEntFeatures, buildFeatures
from coherence import process_example_test
from random import shuffle
import coherence
import subprocess, re
from fbutil import whiteSpace, exclude

def test_phi():
    n = 4
    x1 = pd.Series([0] * n)
    x2 = pd.Series([1] * n)
    p = co.phi([x1, x2], [1, 0])
    
    if p.shape[0] == n * 2:
        print 'iphi is cool'
        print p
    else:
        print 'phi NOT cool'

# test_phi()

def test_PHI():
    
    x0 = pd.Series([1, 1, 1])
    x1 = pd.Series([1, 1, 0])
    x2 = pd.Series([1, 0, 0])
    x3 = pd.Series([0, 0, 0])
    
    p = co.PHI([x0, x1, x2, x3], [0, 1, 2, 3])
#     1,1,1,1,1,0
#     1,1,0,1,0,0
#     1,0,0,0,0,0
#     3,2,1,2,1,0
    print p
    
    
    p = co.PHI([x0, x1, x2, x3], [3, 0, 1, 2])
#     0,0,0,1,1,1
#     1,1,1,1,1,0
#     1,1,0,1,0,0
#     2,2,1,3,2,1
    print p

# test_PHI()

def test_swap_distance():
    order_alt = [1, 3, 0, 2, 4]
#     1,3,0,2,4
#     1,0,3,2,4
#     0,1,3,2,4
#     0,1,2,3,4
#     swap dist = 3
    print co.swap_distance(order_alt)

# test_swap_distance()

def test_loss():
    y_t = [0, 1, 2, 3, 4]
    y_hat = [1, 3, 0, 2, 4]
    
    # loss = 1 - r
    # n = len(y_hat)
    # r = 1 - ( 4 * swap_distance(y_hat) / n * (n - 1) )
    
    # loss =  -1 * ( 4 * swap_distance(y_hat) / n * (n - 1) )
    print co.loss(y_t, y_hat)
    
# test_loss()

def test_s():
    w = pd.Series([1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1])
    x1 = pd.Series([1, 1, 1, 0, 0, 0])
    x2 = pd.Series([0, 0, 0, 1, 1, 1])
    # [0,0,0,1,1,1,1,1,1,0,0,0]
    # [1,0,1,0,1,0,1,0,1,0,1,0]
    # [0,0,0,0,1,0,1,0,1,0,0,0]
    # 3
    print co.s([x1, x2], [1, 0], w), co.s([x1, x2], [0, 1], w)

# test_s()

def test_n():
    y_t = pd.Series([0, 1, 2, 3])
    y_hat = pd.Series([3, 0, 1, 2])
    print 'loss(y_hat)', co.loss(copy.copy(y_hat))
    x0 = pd.Series([1, 1, 1])
    x1 = pd.Series([1, 1, 0])
    x2 = pd.Series([1, 0, 0])
    x3 = pd.Series([0, 0, 0])
    x = [x0, x1, x2, x3] 
    w = pd.Series([5, 0, 3, 2, 1, 0])
    print 'PHI(x,y_t)', co.PHI(x, y_t), 'PHI(x,y_hat)', co.PHI(x, y_hat) 
    print 's(x,y_t,w)', co.s(x, y_t, w), 's(x,y_hat,w)', co.s(x, y_hat, w)
    C = 1
    print 'n', co.n(y_hat, y_t, x, w, C)

# test_n()

def test_highest():
    x0 = pd.Series([1, 1, 1])
    x1 = pd.Series([1, 1, 0])
    x2 = pd.Series([1, 0, 0])
    x3 = pd.Series([7, 7, 7])
    x = [x0, x1, x2, x3]
    
    y0 = [0, 1, 2, 3]
    y1 = [1, 3, 0, 2]
    y2 = [0, 2, 1, 3]
    y3 = [2, 3, 0, 1]
    y = [y0, y1, y2, y3]

    w = pd.Series([1, 1, 1, 2, 2, 2])
    
    print co.highest(x, y, w, 2)
    
# test_highest()

def test_find_ordering():
    x0 = pd.Series([1, 1, 1])
    x1 = pd.Series([1, 1, 0])
    x2 = pd.Series([3, 3, 3])
    x3 = pd.Series([4, 4, 4])
    
    w = pd.Series([7, -7, 4, 4, -10, 3])
    x = [x0, x1, x2, x3]
    shuffle(x)
    print co.findBestOrdering(x, w)
    
# test_find_ordering()

def order_post():
    C = 1
    data = pd.read_csv('../SERU2014_AZ.csv', encoding='utf-8')
    load_as_dataframe = False
    ner = getEntFeatures(load_as_dataframe)
    
    features = buildFeatures(['RUCMOSTIMPTXT'], data)
    print 'built', features.getSize(), 'features'
    
    examples = data['RUCMOSTIMPTXT']
    
    # Unwrap that pickle
    pkl_file = open('w_3.pkl', 'rb')
    w = pickle.load(pkl_file)
    pkl_file.close()
    
    examples['num_sents'] = examples.apply(lambda x: len(nltk.sent_tokenize(unicode(x))))
    examples['y_hat'] = pd.Series()
    
    sent_counter = 0
    y_haters_yo = []
    for index, example in enumerate(examples):
#         if index < 1000:
#             pass
#         else:
        w_n, sent_counter, y_hat = process_example_test(False, copy.deepcopy(w), example, features, sent_counter, ner, C)
        w_n, sent_counter, y_hat_suffle = process_example_test(True, copy.deepcopy(w), example, features, sent_counter, ner, C)
        
        print index, 'real', y_hat, 'shuffled', y_hat_suffle
        
        y_haters_yo.append([y_hat, y_hat_suffle])
        
        if index % 20 == 0:
            # Pickle that shit
            output = open('y_haters_yo_w3_' + str(index) + '.pkl', 'wb')
            pickle.dump(y_haters_yo, output)
            output.close()
             
        
    # Pickle that shit
    output = open('y_haters_yo_w3.pkl', 'wb')
    pickle.dump(y_haters_yo, output)
    output.close()
         
# order_post()      
         
def load_test():
    # Unwrap that pickle
    pkl_file = open('examples.pkl', 'rb')
    examples = pickle.load(pkl_file)
    pkl_file.close()
         
# load_test()       
         
def evaluate_w(haters_pkl):
    pkl_file = open(haters_pkl)
    data = pickle.load(pkl_file)
    pkl_file.close()
    
    len(data)
         
    perfect_pred, perfect_rand = 0, 0
    sum_pred, sum_rand = 0, 0
    total_sents = 0
    adjacent_good_pred, adjacent_bad_pred = 0.0, 0.0
    adjacent_good_rand, adjacent_bad_rand = 0.0, 0.0
    for row in data:
        if row[0] != None:
    #         print row[0]
            for index, _ in enumerate(range(len(row[0]) - 1)):
    #             print index, index  +  1
    #             print row[0][index], row[0][index + 1]
                if (row[0][index + 1] - row[0][index]) == 1:
                    adjacent_good_pred += 1
                else:
                    adjacent_bad_pred += 1
            
            for index, _ in enumerate(range(len(row[0]) - 1)):
    #             print index, index  +  1
    #             print row[1][index], row[1][index + 1]
                if (row[1][index + 1] - row[1][index]) == 1:
                    adjacent_good_rand += 1
                else:
                    adjacent_bad_rand += 1
    #         break
            ideal = range(len(row[0]))
            total_sents += len(row[0])
            ratio_pred = difflib.SequenceMatcher(None, row[0], ideal).ratio()
            sum_pred += ratio_pred * len(row[0])
            ratio_rand = difflib.SequenceMatcher(None, row[1], ideal).ratio()
            sum_rand += ratio_rand * len(row[0])
            if ratio_pred == 1:
                perfect_pred += 1
            if ratio_rand == 1:
                perfect_rand += 1
    # print adjacent_good_pred / (adjacent_good_pred + adjacent_bad_pred)
    
    # print adjacent_good_rand / (adjacent_good_rand + adjacent_bad_rand)
         
    print 'Complete sequence accuracy: predictions', str(int(round(float(perfect_pred) / 
                                                                   len(data), 2) * 100)) + '%', ', baseline', str(int(round(
                float(perfect_rand) / len(data), 2) * 100)) + '%', str(', +' + str(int(round(float(perfect_pred) / perfect_rand - 1, 2) * 100)) + '%'
    )
    
    print 'Sentence accuracy: predictions', str(int(round(float(sum_pred) / total_sents, 2) * 100)) + '%', ', baseline', str(int(round(
                float(sum_rand) / total_sents, 2) * 100)) + '%', str(', +' + str(int(round(float(sum_pred) / sum_rand - 1, 2) * 100)) + '%'
    )
    
    print 'Adjacent sentence accuracy: predictions', str(int(round(adjacent_good_pred / (adjacent_good_pred + adjacent_bad_pred), 2) * 100)) + '%', ', baseline', str(int(round(
                adjacent_good_rand / (adjacent_good_rand + adjacent_bad_rand), 2) * 100)) + '%', str(', +' + str(int(round(
                    adjacent_good_pred / adjacent_good_rand - 1, 2) * 100)) + '%'
    )
         
# evaluate_w('y_haters_yo_w3_900.pkl')

# def testBreakdown():
#     breakdown(range(92))
#     print 'end test breakdown'

# testBreakdown()

def order_via_beam(do_shuffle, w, sents, features, ner):
    feature_vectors = []
    for index, sent in enumerate(sents):
        words = ''.join(ch for ch in sent if ch not in exclude)
        words = re.split(whiteSpace, words)
        feature_vec = [0] * features.getSize()
        feature_vec = coherence.buildFeatureVector(words, features, feature_vec, 0, ner)
        feature_vectors.append(feature_vec)
    if do_shuffle:
        shuffle(feature_vectors)
    y_hat = coherence.findBestOrdering(feature_vectors, w)
    return y_hat
    
def generate_final_ordering():
    
    print 'loading sentences...'
    pkl_file = open('sentences.pkl', 'rb')
    sentencesRaw = pickle.load(pkl_file)
    pkl_file.close()
    # bin40_l50
    sents_index_st = ['1144', '1186', '2186', '2194', '2196', '2688', '2704', '2711', '2946', '2947', '2958', '2966', '2968', '3097', '3126', '3152', '3169', '449', '551', '618', '967', '969', '970', '972']
    # bin30_l10
#     sents_index_st = ['1994', '2251', '2252', '2254', '2264', '2272', '2965', '2966', '3030', '3193', '435', '436', '483', '492', '551', '867']
    sents_index = [int(x) for x in sents_index_st]
    sents = [sentencesRaw[x] for x in sents_index]
    
    print 'loading w...'
    w_f = open('w_3.pkl', 'rb')
    w = pickle.load(w_f)
    w_f.close()
    
    print 'processing features...'
    data = pd.read_csv('../SERU2014_AZ.csv', encoding='utf-8')
    features = buildFeatures(['RUCMOSTIMPTXT'], data)
    print 'built', features.getSize(), 'features'
    load_as_dataframe = False
    ner = getEntFeatures(load_as_dataframe)
     
    print 'starting ordering...'
    order = order_via_beam(False, w, sents, features, ner)
    print sents_index
    print order
    
# generate_final_ordering()


def generate_final_output():
    
    pkl_file = open('sentences.pkl', 'rb')
    sentencesRaw = pickle.load(pkl_file)
    pkl_file.close()
    
    sents_index_st = ['1994', '2251', '2252', '2254', '2264', '2272', '2965', '2966', '3030', '3193', '435', '436', '483', '492', '551', '867']
#     sents_index_st = [1144, 1186, 2186, 2194, 2196, 2688, 2704, 2711, 2946, 2947, 2958, 2966, 2968, 3097, 3126, 3152, 3169, 449, 551, 618, 967, 969, 970, 972]
    sents_index = [int(x) for x in sents_index_st]
    
    order = [5, 8, 12, 0, 9, 13, 14, 15, 1, 2, 3, 4, 6, 7, 10, 11]
#     order = [3, 22, 23, 5, 16, 8, 4, 19, 13, 9, 7, 20, 18, 21, 14, 15, 17, 0, 1, 2, 6, 10, 11, 12]
    
    sents_index_ordered = [sents_index[x] for x in order]
    output = [sentencesRaw[x] for x in sents_index_ordered]
    output_st = ''
    for s in output:
        output_st += ' ' + s
    print output_st

generate_final_output()
