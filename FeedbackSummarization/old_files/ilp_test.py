# from pulp import *

# prob = LpProblem("The Whiskas Problem",LpMinimize)
# #LpVariable("example", None, 100)
# x1=LpVariable("ChickenPercent",0,None,LpInteger)
# x2=LpVariable("BeefPercent",0)

# prob += x1 + x2 == 100, "PercentagesSum"
# prob += 0.100*x1 + 0.200*x2 >= 8.0, "ProteinRequirement"
# prob += 0.080*x1 + 0.100*x2 >= 6.0, "FatRequirement"
# prob += 0.001*x1 + 0.005*x2 <= 2.0, "FibreRequirement"
# prob += 0.002*x1 + 0.005*x2 <= 0.4, "SaltRequirement"

# prob.solve()

# prob.writeLP("WhiskasModel.lp")

# print("Status:", LpStatus[prob.status])

# for v in prob.variables():
#     print(v.name, "=", v.varValue)
#     print v.varValue

# print type(prob), prob.objective
# print("Total Cost of Ingredients per can = ", value(prob.objective))


from pulp import * 

prob = LpProblem("test1", LpMinimize) 

# Variables 
x = LpVariable("x", 0, 4) 
y = LpVariable("y", -1, 1) 
z = LpVariable("z", 0) 

# Objective 
prob += x + 4*y + 9*z 

# Constraints 
prob += x+y <= 5 
prob += x+z >= 10 
prob += -y+z == 7

GLPK().solve(prob) 

# Solution 
for v in prob.variables(): 
	print v.name, "=", v.varValue 

print "objective=", value(prob.objective)  