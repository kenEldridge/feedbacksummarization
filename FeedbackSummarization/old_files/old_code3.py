import pickle, time
from pulp import *
import numpy as np
import pandas as pd

if __name__ == '__main__':
	
	K= 500
	lamda = 0.1 
	pkl_file = open('sentences.pkl', 'rb')
	sentences = pickle.load(pkl_file)
	sentences = sentences[0:10]
	
	pkl_file = open('tuples.pkl', 'rb')
	tuples = pickle.load(pkl_file)
	pkl_file.close()
	
	pkl_file = open("weights_dict_1.pkl", 'rb')
	w = pickle.load(pkl_file)
	pkl_file.close()
	
	tup_list = w.keys()
	w.clear()
	tup_list.sort()
	
	M = np.zeros([len(tuples) + 2,len(tup_list)])
	
	for index, i in enumerate(tuples):
		i = [tuple(x) for x in i]
		if index != 0 and index != M.shape[0]:
			for j in i:
				M[index][tup_list.index(j)] = 1


	prob = LpProblem("Bad problem",LpMaximize)
	s = []
	for i in range(len(sentences)+2):
		s.append(LpVariable('s_'+str(i),cat="Binary"))

	t1 = []
	t1.append((s[0],1))
	for i in range(1,len(sentences)+1):
		t1.append((s[i],len(sentences[i - 1])))
	t1.append((s[len(sentences)+1],1))
	e1 = LpAffineExpression(t1) 
	c1 = LpConstraint(e=e1, sense=-1, name="Constraint 1", rhs=K)

	
	e = []
	for i in range(len(tup_list)):
		e.append(LpVariable('e_'+str(i),cat="Binary"))
	
	c2 = []
	for j in range(len(tup_list)):
		temp = []
		for i in range(len(sentences)+2):
			temp.append((s[i],M[i][j]))
		c2.append(LpConstraint(e=lpSum([LpAffineExpression(temp),-e[j]]), sense=1, name="Constraint 2_" + str(j), rhs=0))

	a = []
	f = []
	start = time.time()
	counter = 0
	for i in range(len(sentences)+2):
		temp = []
		temp1 = []
		for j in range(len(sentences)+2):
			temp.append(LpVariable('a_'+str(i)+'_'+str(j),cat="Binary"))
			temp1.append(LpVariable('f_'+str(i)+'_'+str(j),cat="Binary"))
			if counter % 1000000 == 0:
				print time.time() - start, counter
			counter += 1
		a.append(temp)
		f.append(temp1)
	
	t3 = []
	t4 = []
	t5 = []
	t6 = []
	t7 = []
	t8 = []
	t9 = []
	t10 = []
	t11 = []
	t12 = []
	for i in range(len(sentences)+2):
		t3.append((a[0][i],1))
		t4.append((a[i][0],1))
		t5.append((a[len(sentences)+1][i],1))
		t6.append((a[i][len(sentences)+1],1))
		t9.append((f[0][i],1))
		t10.append((f[i][0],1))

	c7 = []
	c8 = []
	c11 = []
	for j in range(len(sentences)+2):
		temp1 = []
		temp2 = []
		temp3 = []
		temp4 = []
		temp5 = []
		temp6 = []

		temp3.append((f[0][j],1))
		temp6.append((f[j][0],-1))
		for i in range(1,len(sentences)+1):
			temp1.append((a[i][j],1))
			temp2.append((a[i][j],1))
			temp3.append((f[i][j],1))

			temp4.append((a[j][i],1))
			temp5.append((a[j][i],-1))
			temp6.append((f[j][i],-1))
		temp3.append((f[len(sentences)+1][j],1))
		temp6.append((f[j][len(sentences)+1],-1))
		c7.append(LpConstraint(e=lpSum([LpAffineExpression(temp1),LpAffineExpression(temp4),-2*s[j]]), sense=0, name="Constraint 7_" + str(j), rhs=0))
		c8.append(LpConstraint(e=lpSum([LpAffineExpression(temp2),LpAffineExpression(temp5)]), sense=0, name="Constraint 8_" + str(j), rhs=0))
		c11.append(LpConstraint(e=lpSum([LpAffineExpression(temp3),LpAffineExpression(temp6),-s[j]]), sense=0, name="Constraint 11_" + str(j), rhs=0))

	c3 = LpConstraint(e=LpAffineExpression(t3), sense=0, name="Constraint 3", rhs=1)
	c4 = LpConstraint(e=LpAffineExpression(t4), sense=0, name="Constraint 4", rhs=0)
	c5 = LpConstraint(e=LpAffineExpression(t5), sense=0, name="Constraint 5", rhs=0)
	c6 = LpConstraint(e=LpAffineExpression(t6), sense=0, name="Constraint 6", rhs=1)
	c9 = LpConstraint(e=LpAffineExpression(t9), sense=0, name="Constraint 9", rhs=len(sentences))
	c10 = LpConstraint(e=LpAffineExpression(t10), sense=1, name="Constraint 10", rhs=1)
	t13 = []
	t14 = []

	pkl_file = open("weights_dict_1.pkl", 'rb')
	w = pickle.load(pkl_file)
	pkl_file.close()
	for i in range(len(tup_list)):
		t13.append((e[i],w[tup_list[i]]*(lamda)))

	e13 = LpAffineExpression(t13)

	c = pd.read_csv('adjacency_matrix.csv')
	del c['Unnamed: 0']
	c = c.as_matrix()

	for i in range(len(sentences)+2):
		temp = []
		for j in range(len(sentences)+2):
			temp.append((a[i][j],c[i][j]*(1-lamda)))
		t14.append(temp)

	temp_summer = []
	for i in range(len(sentences)+2):
		temp_summer.append(LpAffineExpression(t14[i]))
	e14 = lpSum(temp_summer)

	prob += lpSum([e13,e14])
	prob += c1
	for i in range(len(tup_list)):
		prob += c2[i]

	prob += c3
	prob += c4
	prob += c5
	prob += c6


	print "Constraint 6 added"
	for i in range(1,len(sentences)+1):
		prob += c7[i]
		prob += c8[i]
		prob += c11[i]
	prob += c11[len(sentences)+1]

	prob += c9
	prob += c10
	
	print "Here"
	start = time.time()
	counter = 0
	for i in range(1,len(sentences)+2):
		for j in range(1,len(sentences)+2):
			counter+=1
			if counter % 1000000 == 0:
				print time.time() - start, counter
			cons_12_str = "Constarint 12_"+str(i) +"_"+str(j)
			prob += f[i][j] <= len(sentences) * a[i][j], cons_12_str
	
	prob += s[0] == 1,"Constraint_13"
	prob += s[11] == 1,"Constraint_14"
	print "Wait for it"
	prob.writeLP("summary.lp")
	print "Finished writing to file"
	print pulp.pulpTestAll()
	#prob.solve()
	pulp.GLPK().solve(prob)
	print("Status:", LpStatus[prob.status])
