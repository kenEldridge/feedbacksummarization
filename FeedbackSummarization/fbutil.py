import pandas as pd
import nltk
import pickle, re, string
# import seaborn as sns
from nltk.tag import StanfordNERTagger

whiteSpace = re.compile("\s")
exclude = set(string.punctuation)

class FeatureSet:
    features_index, index_features = {}, {}
    feature_index = 0
    duplicate_count = 0
    
    def setFeature(self, feature):
        if feature not in self.features_index:
            self.features_index[feature] = self.feature_index
            self.index_features[self.feature_index] = feature
            self.feature_index += 1
        else:
            self.duplicate_count += 1
    
    def getFeature(self, index):
        return self.index_features[index]

    def getIndex(self, feature):
        return self.features_index[feature]
    
    def getStats(self):
        return len(self.features_index), self.duplicate_count
    
    def getSize(self):
        return self.getStats()[0]
    
    def printFeatures(self):
        for item in self.features_index:
            print self.getIndex(item), item
            
def showStats(examples):

    examples_stats = pd.DataFrame()
    
    for index, ex in enumerate(examples):
        if len(unicode(ex)) < 3:
            print index, ex
    
    examples_stats['num_chars'] = examples.apply(lambda x: len(unicode(x)))
#     ax1 = sns.distplot(examples_stats['num_chars'])
#     fig1 = ax1.get_figure()
#     fig1.savefig('num_chars.png')
   
    examples_stats['num_words'] = examples.apply(lambda x: len(re.split(whiteSpace, ''.join(ch for ch in unicode(x) if ch not in exclude))))
#     ax2 = sns.distplot(examples_stats['num_words'])
#     fig2 = ax2.get_figure()
#     fig2.savefig('num_words.png')
 
    examples_stats['num_sents'] = examples.apply(lambda x: len(nltk.sent_tokenize(unicode(x))))
    print examples_stats.describe()
#     ax3 = sns.distplot(examples_stats['num_sents'])
#     fig3 = ax3.get_figure()
#     fig3.savefig('num_sents.png')

#     sns.plt.show()

# def buildNamedEnt(sents, ner):
#     # english.all.3class.distsim.crf.ser.gz
#     # english.conll.4class.distsim.crf.ser.gz
#     # english.muc.7class.distsim.crf.ser.gz
#     st = StanfordNERTagger('english.all.3class.distsim.crf.ser.gz') 
# 
#     for sent in sents:
#         words = ''.join(ch for ch in sent if ch not in exclude)
#         words = re.split(whiteSpace, words)
#         ners = st.tag(words) 
#         ner.append(ners)
#         
#     return ner

def makeNamedEntPickle(features_cols, data):

    st = StanfordNERTagger('english.all.3class.distsim.crf.ser.gz') 

    counter = 0
    ner = []
    for col in features_cols:
        data_sub = data[col]
        showStats(data_sub)
        for example in data_sub:
            if type(example) == unicode and len(example) > 2:
                
                sents = nltk.sent_tokenize(example)
                
                for sent in sents:
                    words = ''.join(ch for ch in sent if ch not in exclude)
                    words = re.split(whiteSpace, words)
                    ners = st.tag(words) 
                    ner.append(ners)
                    counter += 1
                
                if counter % 100 == 0:
                    print counter

    #Pickle that shit
    output = open('ner.pkl', 'wb')
    pickle.dump(ner, output)
    output.close()
