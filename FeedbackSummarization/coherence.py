import pandas as pd
import numpy as np
import time, re
import nltk, pickle
from fbutil import whiteSpace, exclude
from features import buildFeatures, getEntFeatures, buildFeatureVector
from fbutil import makeNamedEntPickle
import copy, sys
from random import shuffle

#input: set of two feature vectors, order of the vecs
#output: concatenation of the two vecs
def phi(vec_set, set_order):
    phi = []
    for i in set_order:
        phi.append(vec_set[i])
    return pd.concat(phi)

def phi_l(vec_set, set_order):
    phi = []
    for i in set_order:
        phi.append(vec_set[i])
    return np.concatenate([phi[0], phi[1]])
    
#input: set of n feature vectors, order of the vecs
#output: sum of phi() for each set of adjacent vectors
def PHI(feature_vectors, order):
    P = []
    for index, item in enumerate(order):
#         print item, order[index+1]
        x1 = feature_vectors[item]
        x2 = feature_vectors[order[index+1]]
        
        P.append(phi_l([x1,x2],[0,1]))
        
        if len(order) == index + 2:
            break
    
    psum = [0] * len(feature_vectors[0]) * 2
    for p in P:
        psum = map(sum, zip(psum,p))
    
    return np.array(psum)

def swap( A, x, y ):
    tmp = A[x]
    A[x] = A[y]
    A[y] = tmp
    
#input: alternative orderings
#output: number of swaps of adjacent element
#needed to bring the vectors into alignment
#This is just bubble sort.  One of the vectors will
#always be in sequential order.  So all we need to
#do is write a simple bubble sort and count. 
def swap_distance(order_alt):
    order_alt = copy.deepcopy(order_alt)
    dist = 0
    
    #Bubble sort shamelessly lifted from: http://www.geekviewpoint.com/python/sorting/bubblesort
    for i in range( len( order_alt ) ):
        for k in range( len( order_alt ) - 1, i, -1 ):
            if ( order_alt[k] < order_alt[k - 1] ):
                swap( order_alt, k, k - 1 )
                dist += 1
    
    return dist

#input: alternative orderings
#output: loss float
def loss(y_hat):
    n = len(y_hat)
    denom = n * (n - 1)
    return -1 * ( 4.0 * swap_distance(y_hat) / denom )

#input: set of two feature vectors, order of the vecs, w
#output: w dot PHI(x,y)
def s(x,y,w):
    return w.dot(PHI(x,y))

#input: y_hat, y_t, x, w, C (hyper-param)
#output: float
def n(y_hat, y_t, x, w, C):
    num = loss(y_hat) - s(x,y_t,w) + s(x,y_hat,w)
    den = ((PHI(x,y_t) - PHI(x,y_hat))**2).sum() + (1.0/2*C)
    return float(num) / den
    
def highest(x, y_list, w, beam_size):
    orderings = pd.DataFrame()
    idx = []
    the_lists = []
    s_val = []
    for index, y in enumerate(y_list):
        idx.append(index)
        the_lists.append(y)
        s_val.append(s(x,y,w))
    orderings['y_index'] = pd.Series(idx)
    orderings['y'] = pd.Series(the_lists)
    orderings['s'] = pd.Series(s_val)
    orderings = orderings.sort_values(by='s', ascending=False)
#     print orderings
    return orderings[0:beam_size]['y']
    
#input: set of vectors to order
#output: potential orderings that maximizes s(x,y,w)
#use beam search scored by s(x,y,w)
def findBestOrdering(feature_vectors, w):
    #make each sentence a starting state
    #for each add all potential seconds states
    #retain the k highest chains
    #repeat until you have n nodes in each chain
    beam_size = 80
    length = len(feature_vectors)
    S = range(length)

    remainder = []
    next_orders = []
    for item in S:
        fixed_bits = [item]
        remainder = [x for x in S if x not in fixed_bits]
        order = fixed_bits + remainder
        next_orders.append(order)
    orders = next_orders
    orders = highest(feature_vectors, orders, w, beam_size)

    for index, _ in enumerate(range(length - 1)):
        next_orders = []
        print 'processing', index, 'of', length
        for order in orders:
            fixed_bits = order[0:index + 1]
            remainder = [x for x in S if x not in fixed_bits]
            for item in remainder:
                sub_order = fixed_bits + [item]
                smaller_remainder = [x for x in S if x not in sub_order]
                sub_order = sub_order + smaller_remainder
                next_orders.append(sub_order)
        orders = next_orders
#         print orders
        orders = highest(feature_vectors, orders, w, beam_size)
#         print orders
    return orders.iloc[0]
    
#input: w, x, y_t, y_hat, C
#output: new w
def update_w(w, x, y_t, y_hat, C):
    return w + n(y_hat, y_t, x, w, C) * (PHI(x, y_t) - PHI(x, y_hat))

def process_example(w, example, features, sent_counter, ner, C):
    y_hat = None
    if type(example) == unicode and len(example) > 2:
        sents = nltk.sent_tokenize(example)
        if len(sents) > 1:
            feature_vectors = []
            for sent in sents:
                words = ''.join(ch for ch in sent if ch not in exclude)
                words = re.split(whiteSpace, words)
                
                feature_vec = [0] * features.getSize()
                feature_vec = buildFeatureVector(words, features, feature_vec, sent_counter, ner)
                feature_vectors.append(feature_vec)
                sent_counter += 1
            
            y_hat = findBestOrdering(feature_vectors, w)
            y_t = list(range(len(feature_vectors)))
            w = update_w(w, feature_vectors, y_t, y_hat, C)
        else:
            sent_counter += len(sents)
    
    return w, sent_counter

def process_example_test(do_shuffle, w, example, features, sent_counter, ner, C):
    y_hat = None
    if type(example) == unicode and len(example) > 2:
        sents = nltk.sent_tokenize(example)
        if len(sents) > 1:
            feature_vectors = []
            for sent in sents:
                words = ''.join(ch for ch in sent if ch not in exclude)
                words = re.split(whiteSpace, words)
                
                feature_vec = [0] * features.getSize()
                feature_vec = buildFeatureVector(words, features, feature_vec, sent_counter, ner)
                feature_vectors.append(feature_vec)
                sent_counter += 1
            if do_shuffle:
                shuffle(feature_vectors)
            y_hat = findBestOrdering(feature_vectors, w)
#             y_t = list(range(len(feature_vectors)))
#             w = update_w(w, feature_vectors, y_t, y_hat, C)
        else:
            sent_counter += len(sents)
    
    return w, sent_counter, y_hat

#input: all feature vectors
#output: w (length = 2 * size of one feature vector)
def buildW(examples, features):
    w_seed, C , max_it = 1, 1, 3
    w = pd.Series([w_seed] * 2 * features.getSize())
    load_as_dataframe = False
    ner = getEntFeatures(load_as_dataframe)
    start = time.time()
    outof = max_it * len(examples)
    counter = 0
    for oindex, _ in enumerate(range(max_it)):
        sent_counter = 0
        for index, example in enumerate(examples):
            w, sent_counter = process_example(w, example, features, sent_counter, ner, C)
            counter += 1
            if counter % 1000 == 0:
                #Pickle that shit
                print time.time() - start, counter, 'of', outof
                output = open('w_' + str((oindex+1)*index) + '_5.pkl', 'wb')
                pickle.dump(w, output)
                output.close()
#             if index > 1000:
#                 break
            
    return w

def createAdjacencyMatrix(sentences, features, w):
    s_features = []
    load_as_dataframe = False
    ner = getEntFeatures(load_as_dataframe)
    sent_counter = 0
    for sent in sentences:
        words = ''.join(ch for ch in sent if ch not in exclude)
        words = re.split(whiteSpace, words)
        feature_vec = [0] * features.getSize()
        feature_vec = buildFeatureVector(words, features, feature_vec, sent_counter, ner)        
        feature_vec = feature_vec.values
        s_features.append(feature_vec)

    adjacencyMatrix = []
    counter = 0
    start = time.time()
    for s_i in s_features:
        row = []
        for s_j in s_features:
            p = phi_l([s_i, s_j],[0, 1])
#             p = p.reset_index()
#             del p['index']
            row.append(w.dot(p))
            counter += 1
            if counter % 100000 == 0:
                print time.time() - start, counter
        adjacencyMatrix.append(row)

    print time.time() - start
    
    print pd.DataFrame(adjacencyMatrix).shape
    
    output = open('adjacency_matrix.pkl', 'wb')
    pickle.dump(adjacencyMatrix, output)
    output.close()



