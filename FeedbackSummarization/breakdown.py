import math, pickle, re, subprocess
from code3 import buildLP
import pandas as pd

def runGLPSOL(counter):
	subprocess.call(['glpsol','--cpxlp', 'summary_' + str(counter) + '.lp', '--output', 'summary_' + str(counter) + '.out']) 

def parseOutput(counter):
	sents = []
	a = {}
	s = []
	max_sent = 0
	filename = "summary_"+str(counter)+".out"
	#filename = "summary.out"
	with open(filename, 'rb') as f:
		for line in f:
			words=line.split()
			if(len(words)>5):
				if(words[1][0]=='a'):
					tags=re.split('_',words[1])
					if(words[3]=='1'):
						a[tags[1]]=tags[2]
				
				if(words[1][0]=='s'):
					tags=re.split('_',words[1])
					if(words[3]=='1'):
						s.append(tags[1])
					max_sent+=1
	print s
# 	print a
# 	print max_sent

# 	current = "s"
# 	end = "t"
# 	while (current!=end):
# 		current = a[current]
# 		sents.append(current)
# 
# 	sents = sents[:-1]
# 	for i in range(len(sents)):
# 		sents[i] = int(sents[i])
# 	print sents
	return s

def generateTextSummary():
	pass

# sentences is a list of indecies in the master sentences list.
# initially it will be equal to range(len(sentences))
def breakdown(sentences, sentencesRaw, tuples, w, c):
	counter, bin_size = 0, 30
	if len(sentences) > bin_size:
		number_bins = int(math.ceil(float(len(sentences))/bin_size))
		subsets = []
		for index_i, _ in enumerate(range(number_bins)):
			ssub = []
			for index_j, _ in enumerate(range(bin_size)):
				if index_i * bin_size + index_j < len(sentences):
					ssub.append(sentences[index_i * bin_size + index_j])
			subsets.append(ssub)
		remaining_sents = []
		for s in subsets:
			#BuildLP basically executes code3.
			#We'll need to make sure the variable names for the lp problem
			#use the index in the master sentence list and not the 
			#subset.
			lp_status = buildLP(s, counter, sentencesRaw, tuples, w, c)
			print lp_status
			#runGLPSOL makes a system call to run the solver
			runGLPSOL(counter)
			#parseOutput reads the result of runGLPSOL from text files
			sents = parseOutput(counter)
			#list concatenation
			remaining_sents += sents
			counter += 1
		#recursive call
		breakdown(remaining_sents)
	else:
		buildLP(sentences, counter)
		runGLPSOL(counter)
		sents = parseOutput(counter)
		generateTextSummary(sents)


def main():
	
	print 'loading sentences...'
	pkl_file = open('sentences.pkl', 'rb')
	sentencesRaw = pickle.load(pkl_file)
	pkl_file.close()

	print 'loading tuples...'
	pkl_file = open('tuples.pkl', 'rb')
	tuples = pickle.load(pkl_file)
	pkl_file.close()
	
	print 'loading weights...'
	pkl_file = open("weights_dict_1.pkl", 'rb')
	w = pickle.load(pkl_file)
	pkl_file.close()
	
	print 'loading adjacency matrix...'
	c = pd.read_csv('adjacency_matrix.csv')
	del c['Unnamed: 0']
	c = c.as_matrix()
	
	print 'starting breakdown...'
	breakdown(range(len(sentencesRaw)), sentencesRaw, tuples, w, c)
	
main()
	