import pandas as pd
import numpy as np
import nltk
from nltk.util import ngrams
import re, pickle
from nltk.corpus import words
from fbutil import whiteSpace, exclude
from fbutil import FeatureSet, showStats

def buildNgrams(sents, n, features):
    
    for sent in sents:
        words = ''.join(ch for ch in sent if ch not in exclude)
        words = re.split(whiteSpace, words)
        grams = ngrams(words, n)
        for gram in grams:
            gram_st = ''
            for i in range(n):
                gram_st += gram[i].lower() + '_'
            features.setFeature(gram_st)
   
def hydrateNgrams(words, n, features, feature_vec):
    
    grams = ngrams(words, n)
    for gram in grams:
        gram_st = ''
        for i in range(n):
            gram_st += gram[i].lower() + '_'
        feature_index = features.getIndex(gram_st)
        feature_vec[feature_index] = 1
    return feature_vec

def buildPOS(sents, features):
    
    for sent in sents:
        words = ''.join(ch for ch in sent if ch not in exclude)
        words = re.split(whiteSpace, words)
        words = [x for x in words if len(x) > 0]
        poss = nltk.pos_tag(words)
        for pos in poss:
            pos_feature = pos[1] + '_pos_tag'
            features.setFeature(pos_feature)
    
def hydratePOS(words, features, feature_vec):
    
    words = [x for x in words if len(x) > 0]
    poss = nltk.pos_tag(words)
    for pos in poss:
        pos_feature = pos[1] + '_pos_tag'
        feature_index = features.getIndex(pos_feature)
        feature_vec[feature_index] = 1
            
    return feature_vec

def getEntFeatures(df):
    # Unwrap that pickle
    pkl_file = open('ner.pkl', 'rb')
    ner = pickle.load(pkl_file)
    pkl_file.close()
    if df:
        ner = pd.DataFrame(ner)
    return ner   
    
def buildNamedEntFeatures(features):
    load_as_df = True
    ner_df = getEntFeatures(load_as_df)
    for ners in range(ner_df.shape[0]):
        for ner in ner_df.iloc[ners]:
            if type(ner) != type(None) and ner[1] != 'O':
                features.setFeature(ner[1] + '_ner')

def hydrateNamedEnt(words, features, feature_vec, ner, sent_counter):
    ners = ner[sent_counter]
    for ner in ners:
        if type(ner) != type(None) and ner[1] != 'O':
            ner_feat = ner[1] + '_ner'
            feature_index = features.getIndex(ner_feat)
            feature_vec[feature_index] = 1
                
    return feature_vec

def buildFeatures(features_cols, data):
    examples_list = []
    features = FeatureSet()
    
    buildNamedEntFeatures(features)

    sent_counter = 0
    for col in features_cols:
        data_sub = data[col]
#         showStats(data_sub)
        for example in data_sub:
            if type(example) == unicode:
                
                sents = nltk.sent_tokenize(example)
                
                buildNgrams(sents, 1, features)
                buildNgrams(sents, 2, features)
                buildPOS(sents, features)
                sent_counter += len(sents)
                examples_list.append(sents)

    output = open('examples_list.pkl', 'wb')
    pickle.dump(examples_list, output)
    output.close()
    return features

def buildFeatureVector(words, features, feature_vec, sent_counter, ner):
    feature_vec = hydrateNgrams(words, 1, features, feature_vec)
    feature_vec = hydrateNgrams(words, 2, features, feature_vec)
    feature_vec = hydratePOS(words, features, feature_vec)
    feature_vec = hydrateNamedEnt(words, features, feature_vec, ner, sent_counter)
    return np.array(feature_vec)

def getFeatureMatrix(features_cols, data, features, load_from_pkl):
    features_matrix = None
    if load_from_pkl:
        pkl_file = open('features.pkl', 'rb')
        features_matrix = pickle.load(pkl_file)
        pkl_file.close()
    else:
        features_matrix = buildFeatureMatrix(['RUCMOSTIMPTXT'], data, features)
#         output = open('features.pkl', 'wb')
#         pickle.dump(features_matrix, output)
#         output.close()
    return features_matrix

def buildFeatureMatrix(features_cols, data, features):
    
    featre_matrix = []
    
    load_as_dataframe = False
    ner = getEntFeatures(load_as_dataframe)
    
    counter, sent_counter = 0, 0
    for col in features_cols:
        data_sub = data[col]
        for example in data_sub:
            if type(example) == unicode and len(example) > 2:
                sents = nltk.sent_tokenize(example)
                sent_counter += len(sents)
                for sent in sents:
                    words = ''.join(ch for ch in sent if ch not in exclude)
                    words = re.split(whiteSpace, words)
                    
                    feature_vec = [0] * features.getSize()
                    feature_vec = buildFeatureVector(words, features, feature_vec, sent_counter, ner)
                    featre_matrix.append(feature_vec)
                
                    counter += 1
                    if counter % 100 == 0:
                        print 'counter:', counter
    
    return pd.DataFrame(featre_matrix)